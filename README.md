# forecaster-app

- To Install:
    - npm install
  
- To Build: 
    - npm run build

- To Start locally:
    - npm run develop

- To Test locally:
    - npm run test

-   Build folder: 
    -   dest
  
# Live Site: https://pedantic-borg-606363.netlify.com/

Note: This is published to Netifly via continuous build against Master branch. 

# Stack
- React 16
- ES6/ES7 + babel 
- MomentJS
- Bulma 
- Jest + Enzyme
- Webpack
- Adobe XD (Wireframing/mocking)  

# Comments
  - VCS Strategy: Added a develop branch. And branch feature branches from this, merge, then raise PR against master when story completed.
  - Site is responsive for all viewports 
  - City information is provided via the local JSON file from openweather (Advised city id yields most accurate results + for user searching of locale)

# Acknowledged Bugs
  - When user searches a city and the 'too many results' tag is shown, this breaks as its not passing a city ID. 
    - Solution: See future changes

# Future Changes:

- Unit switching
    - Allow user to define units. Default is Metric. 
      - How? I would update the api url param for unit. I would add a toggle in the CitySelector component and in addition to passing the cityID in the callback, I would include the metric unit. 

- Data formatting
    - Would have standardised the data formats in the backend
      - How? Added a express node backend which handles the api calls. Inc. exposing new endpoints. Then I can clean the data being passed into a more standard format across components.

- City Selector
    - When too many results returned, the tag stating 'too many results' isnt a valid city. So if user clicked it would break the page. 
      - How? Either handle it in the api function, where if value = 'toomanyresults', ignore. OR - simply render a text element stating too many results. 

- City Selector Data
    - Cities json information is via the unpacked cities json provided by weather data provider.
      - How? Add the local gzip version of the cities, and unpack this during build steps. Reduces 18mb file to 1mb.