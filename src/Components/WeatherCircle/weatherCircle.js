import './css/weatherCircle.css';
import React from "react";
import ReactDOM from "react-dom";
var moment = require('moment');

export class WeatherCircle extends React.Component {
    render() {
      return (
        <div className="circleContainer">
            <div className="circleContents">{moment(this.props.data).format("DD ddd")}</div>
        </div>    
      )
    }
  }