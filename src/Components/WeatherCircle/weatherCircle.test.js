import React from "react";
import { shallow, mount, render } from 'enzyme';
import { WeatherCircle } from './weatherCircle';

it('WeatherCircle renders correctly with no props', () => {
    const component = shallow(<WeatherCircle/>);
    expect(component).toMatchSnapshot();
  });

  it('WeatherCircle renders a date in the date field', () => {
    const wrapper = shallow(<WeatherCircle data={'2019-01-30 09:00:00'}/>);
    const date = wrapper.find('.circleContents'); 

    expect(date.text()).toEqual('30 Wed');
  });