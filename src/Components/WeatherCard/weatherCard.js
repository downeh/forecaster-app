import './css/weatherCard.css';
import React from "react";
import ReactDOM from "react-dom";
import { Box, Columns, Heading, Content, Card } from 'react-bulma-components/full';
var moment = require('moment');
let boxCollection = [];

let boxDetail = (value) => {
  return <div key={value[0].dt_txt} style={{marginBottom:'2rem', textAlign:'center'}}>
            <Heading size={3} style={{fontWeight:'300'}}>{moment(value[0].dt_txt).format("HH:mm")}</Heading>
              <Content style={{fontWeight:'300'}}>
                <Box>
                <p><strong>Temperature</strong></p>
                <p><i>{value[0].main.temp}</i> </p>
                <p><strong>Conditions</strong></p>
                <p><i>{value[0].weather[0].main}</i></p>
                <p><strong>Wind</strong></p>
                <p><i>{value[0].wind.speed}</i></p>
                </Box>
            </Content>
         </div>
}

export class WeatherCard extends React.Component {
  componentWillMount() {
    boxCollection = [];

    for (var value of this.props.data) {
      let box;
      box = boxDetail(value)
      boxCollection.push(box)
    }
  } 
    render() {
      return (
          <div>{boxCollection}</div>  
      )
    }
  }