let formattedResultArr=[];
var moment = require('moment');
var citiesJSON = require('./current.city.list.json');

export async function getForecast(cityID) {
    formattedResultArr = [];

   let conn = await fetch('https://api.openweathermap.org/data/2.5/forecast?id=' + cityID + '&units=metric&appid=2e863ec7cc1959cfe42d6488aed57002');
   let result = await conn.json();
    await result.list.map(groupByDate);

    let todayWeather = {};
    let currPage = 0;
    await Object.keys(formattedResultArr).forEach(e => {
        if(currPage == 0) {
          todayWeather = formattedResultArr[e];
          currPage++;
        }
      });

     let weatherData = ({'Today':todayWeather, 'Forecast':formattedResultArr});
  return weatherData
  }

function groupByDate(value, index, array) {
    let d = moment(value['dt_txt']).format("YYYY-MM-DD")
    formattedResultArr[d]=formattedResultArr[d]||[];
    formattedResultArr[d].push([value]);
}

export async function getCities(receivedValue) {
   let arrayFound = citiesJSON.filter(x => x.name.toLowerCase().includes(receivedValue.toLowerCase()))
   if(arrayFound.length < 1) {
     arrayFound=[]
     return arrayFound;
   }
  return arrayFound;
}