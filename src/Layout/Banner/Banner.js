import './css/banner.css';
import React from "react";
import ReactDOM from "react-dom";
import { Section, Heading, Hero, Container, Columns } from 'react-bulma-components/full';
var moment = require('moment');

export class Banner extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
      return (
        <Section>
            <Hero color="primary" gradient>
                <Hero.Body>
                    <Container>
                        <Columns className="currentWeatherDetails">
                            <Columns.Column size={6}>
                                <Heading>CURRENT WEATHER</Heading>
                                <Heading subtitle size={5}>{moment(this.props.data.dt_txt).format("DD-MM-YYYY")}</Heading>
                                <Heading subtitle size={5}>Forecast @ {moment(this.props.data.dt_txt).format("HH:mm a")}</Heading>
                            </Columns.Column>
                            <Columns.Column size={6} className="currentWeatherDetails">
                                <Heading>Temperature <i>{this.props.data.main.temp}</i> </Heading>
                                <Heading>Conditions <i>{this.props.data.weather[0].main}</i> </Heading>
                                <Heading>Wind <i>{this.props.data.wind.speed}</i></Heading>
                            </Columns.Column>
                        </Columns>
                    </Container>
                </Hero.Body>
            </Hero>
      </Section>
      )
    }
  }