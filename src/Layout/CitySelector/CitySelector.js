import './css/citySelector.css';
import React from "react";
import ReactDOM from "react-dom";
import { Navbar, Heading, Modal, Section, Tag } from 'react-bulma-components/full';
import { getCities } from '../../api/weatherAPI';

export class CitySelector extends React.Component {
  constructor() {
    super();
    this.state = {
      input:'',
      results:[]
    }
  }

  componentWillMount() {
      this.setState({show:this.props.showOnStart})
  }

  open = () => this.setState({ show: true });
  close = () => this.setState({ show: false });

  preflightSearch(e) {
    e.preventDefault();
    this.setState({input:e.target.value})
    let returnedCityMatches = getCities(e.target.value);

    returnedCityMatches.then((result) => {
      if(result.length > 50) {
        this.setState({
          results:[{name:'Too Many Matches', 
          id:'TooManyMatches', 
          status:'danger'}]
        })
      } else {
        this.setState({
          results:result
        })
      }
  });
  }

  applySelection(e) {
    let selectedCityName = e.target.innerHTML;
    let selectedCityID = e.target.id;
    this.props.callback(selectedCityID, selectedCityName)
    this.setState({show:false, selectedCity:selectedCityName});
  }

  render() {
    return (
      <div><Navbar className="cityTitle">
          <Navbar.Container>
          <Navbar.Item href="#"><Heading onClick={this.open} size={3} style={{fontWeight:'300'}}>{this.state.selectedCity}</Heading></Navbar.Item>
          </Navbar.Container>
          <Navbar.Container position="end">
          <Navbar.Item href="#"><Heading onClick={this.open} size={3} style={{color:'#e66e00'}}>Change City</Heading></Navbar.Item>
          </Navbar.Container>
          </Navbar>
          <Modal show={this.state.show} onClose={this.close} modal={{ closeOnBlur: true, showClose: false }}>
          <Modal.Content>
            <Section id="searchSection" style={{backgroundColor:'white'}}>
              <input onChange={(e) => this.preflightSearch(e)} value={this.state.input} placeholder="City Name...." autoFocus></input>
              <Tag.Group>

                {this.state.results.map(city => (
                  <a key={city.id} 
                    id={city.name} 
                    onClick={(e) => this.applySelection(e)
                  }>
                <Tag id={city.id} color={city.status}>{city.name}</Tag></a>
              ))}

              </Tag.Group>
            </Section>
          </Modal.Content>
          </Modal>
      </div>
    )
  }
}