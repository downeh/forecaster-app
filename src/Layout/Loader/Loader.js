import './css/loader.css';
import React from "react";
import ReactDOM from "react-dom";
import { Heading } from 'react-bulma-components/full';

export class Loader extends React.Component {
    render() {
      return (
        <div className="menuContainer">
          <div className="menuForm">
            <Heading>Working on that now....</Heading>
          </div>   
        </div>
      )
    }
  }