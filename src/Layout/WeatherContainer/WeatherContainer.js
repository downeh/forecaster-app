import './css/weatherContainer.css';
import React from "react";
import ReactDOM from "react-dom";
import { Section, Columns, Heading } from 'react-bulma-components/full';
import { Banner } from '../Banner/Banner';
import { WeatherCard } from '../../Components/WeatherCard/weatherCard';
import { WeatherCircle } from '../../Components/WeatherCircle/weatherCircle';
import { getForecast } from '../../api/weatherAPI';
import { Loader } from '../Loader/Loader';
import { CitySelector } from '../CitySelector/CitySelector';

let cardCollection = [];

let cardElement = (objCollection, e) => {
  return <Columns.Column key={e}>
            <WeatherCircle data={e} />
            <WeatherCard data={objCollection[e]} />
          </Columns.Column>
}

export class WeatherContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      dataLoading:false,
      weatherToday:[],
      showOnStart:true,
    }
  }
  
  async getData(cityKey, cityName) {
    cardCollection = [];
    this.setState({dataLoading:true})
    let dataCall = await getForecast(cityKey)
    let forecastCollection = dataCall.Forecast;

    Object.keys(forecastCollection).forEach(e => {
      let generateCard = cardElement(forecastCollection, e)
      cardCollection.push(generateCard);
    })

    this.setState({ 
     weatherToday:dataCall.Today, 
     weatherForecast:dataCall.Forecast, 
     dataLoading:false, 
     selectedCity:cityName
    })
  }

    render() {
      if(this.state.dataLoading) {
        return(<Loader />)
      } else if(this.state.weatherToday.length < 1) {
        return(
          <CitySelector showOnStart={true} callback={(cityID, cityName) => { this.getData(cityID, cityName)}} />
        )
      } 
      else {
        return (
          <Section>
              <CitySelector showOnStart={false} callback={(cityID, cityName) => { this.getData(cityID, cityName)}} />
              <Heading size={1}>{this.state.selectedCity}</Heading>
              <Banner data={this.state.weatherToday[0][0]} />
              <Columns>{cardCollection}</Columns>
          </Section>
        )
    }
  }
  }