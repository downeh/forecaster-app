import React from 'react';
import ReactDOM from 'react-dom';
import { Section, Container, Columns } from 'react-bulma-components/full';
import { WeatherContainer } from './Layout/WeatherContainer/WeatherContainer';

export class Main extends React.Component {
    render() {
        return(
            <Container>
                <WeatherContainer />
            </Container>
        )}
}

